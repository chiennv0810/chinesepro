//
//  WordsEntity.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/26/20.
//

import Foundation
import SQLite

class WordsEntity{
    static let share = WordsEntity()
    
    private let tbl_words = Table("tbl_words")
    private let id = Expression<Int>("_id")
    private let nativeContent = Expression<String>("native_content")
    private let targetContent = Expression<String>("target_content")
    private let idCategory = Expression<Int>("id_category")
    private let aux = Expression<Int>("aux")

    func getData(idCate:Int) -> [WordsModel]{
        var listData = [WordsModel]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.tbl_words.filter(self.idCategory == idCate)) {
                for item in listCate {
                    listData.append(WordsModel(id: item[id], nativeContent: item[nativeContent], targetContent: item[targetContent], idCategory: item[idCategory], aux: item[aux]))
                }
            }
        } catch {
            print("Cannot get data from \(self.tbl_words), Error is: \(error)")
        }
        return listData
    }
    
    func updateAUX(idP:Int, auxdata:Int){
        if auxdata == 0 {
            do {
                let updateAUX = tbl_words.filter(id == idP)
                if (try Sqldata.shared.connection?.run(updateAUX.update(aux <- 1)))! > 0 {
                    print("updated alice")
                } else {
                    print("alice not found")
                }
            } catch {
                print("update failed: \(error)")
            }
        } else {
            do {
                let updateAUX = tbl_words.filter(id == idP)
                if (try Sqldata.shared.connection?.run(updateAUX.update(aux <- 0)))! > 0 {
                    print("updated alice")
                } else {
                    print("alice not found")
                }
            } catch {
                print("update failed: \(error)")
            }
        }

    }
    
    func getDataFavorite()-> [WordsModel]{
        var listData = [WordsModel]()
        do {
            if let listFV = try  Sqldata.shared.connection?.prepare(self.tbl_words.filter(self.aux == 1)) {
                for item in listFV {
                    listData.append(WordsModel(id: item[id], nativeContent: item[nativeContent], targetContent: item[targetContent], idCategory: item[idCategory], aux: item[aux]))
                }
            }
        } catch {
            print("Cannot get data from \(self.tbl_words), Error is: \(error)")
        }
        return listData
    }

    
}
