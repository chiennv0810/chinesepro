//
//  CategoryEntity.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/25/20.
//

import Foundation
import SQLite

class CategoryEntity{
    static let shared = CategoryEntity()
    
    private let tbl_category = Table("tbl_categories")
    private let id = Expression<Int>("_id")
    private let name = Expression<String>("name")
    private let image_name = Expression<String>("image_name")
    private let image_info = Expression<String>("image_info")
    
    
    func getData() -> [CategoryModel]{
        var listData = [CategoryModel]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.tbl_category) {
                for item in listCate {
                    listData.append(CategoryModel(id:item[id],name: item[name], image_name: item[image_name],image_info: item[image_info]))
                }
            }
        } catch {
            print("Cannot get data from \(self.tbl_category), Error is: \(error)")
        }
        return listData
    }
}
