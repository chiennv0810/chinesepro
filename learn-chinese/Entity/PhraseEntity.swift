//
//  PhraseEntity.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/26/20.
//

import Foundation
import SQLite

class PhraseEntity{
    static let share = PhraseEntity()
    
    private let tbl_phrases = Table("tbl_phrases")
    private let id = Expression<Int>("_id")
    private let nativeContent = Expression<String>("native_content")
    private let targetContent = Expression<String>("target_content")
    private let idCategory = Expression<Int>("id_category")
    private let aux = Expression<Int>("aux")

    func getData(idCate:Int) -> [PhraseModel]{
        var listData = [PhraseModel]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.tbl_phrases.filter(self.idCategory == idCate)) {
                for item in listCate {
                    listData.append(PhraseModel(id: item[id], nativeContent: item[nativeContent], targetContent: item[targetContent], idCategory: item[idCategory], aux: item[aux]))
                }
            }
        } catch {
            print("Cannot get data from \(self.tbl_phrases), Error is: \(error)")
        }
        return listData
    }
    
    func updateAUX(idP:Int, auxdata:Int ){
        if auxdata == 0 {
            do {
                let updateAUX = tbl_phrases.filter(id == idP)
                if (try Sqldata.shared.connection?.run(updateAUX.update(aux <- 1)))! > 0 {
                    print("updated alice")
                } else {
                    print("alice not found")
                }
            } catch {
                print("update failed: \(error)")
            }
        } else {
            do {
                let updateAUX = tbl_phrases.filter(id == idP)
                if (try Sqldata.shared.connection?.run(updateAUX.update(aux <- 0)))! > 0 {
                    print("updated alice")
                } else {
                    print("alice not found")
                }
            } catch {
                print("update failed: \(error)")
            }
        }
        
     
    }
    
    func getDataFavorite()-> [PhraseModel]{
        var listData = [PhraseModel]()
        do {
            if let listFV = try  Sqldata.shared.connection?.prepare(self.tbl_phrases.filter(self.aux == 1)) {
                for item in listFV {
                    listData.append(PhraseModel(id: item[id], nativeContent: item[nativeContent], targetContent: item[targetContent], idCategory: item[idCategory], aux: item[aux]))
                }
            }
        } catch {
            print("Cannot get data from \(self.tbl_phrases), Error is: \(error)")
        }
        return listData
    }
    
    
    
}
