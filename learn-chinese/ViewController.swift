//
//  ViewController.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/20/20.
//

import UIKit
import SwiftKeychainWrapper

class ViewController: UIViewController {
    var vSpinner: UIView?
    
    @IBOutlet weak var outletBtnNextScreen: UIButton!
    @IBOutlet weak var outletTwoline: UIButton!
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var lblSelected: UILabel!
    
    @IBOutlet var listLbl:[UILabel]!
    
    var nameLocal:String = "en"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if PaymentManager.shared.isPurchase(){
//
//        }else{
//            DispatchQueue.main.async {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
//            }
//        }
        
        print("SSS: "+String(UserDefaults.standard.string(forKey: "languageKey") ?? "abc"))
        
        let sss = UserDefaults.standard.string(forKey: "languageKey") ?? "abc"
        if sss == "abc"{
            UserDefaults.standard.set("en", forKey: "languageKey")
        }
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            outletBtnNextScreen.titleLabel?.font = outletBtnNextScreen.titleLabel?.font.withSize(25)
            outletTwoline.setImage(UIImage(named: "twoline2xblack"), for: .normal)
        }
        
        imgSelected.layer.shadowRadius = 8
        imgSelected.layer.shadowOffset = .zero
        imgSelected.layer.shadowOpacity = 0.7
        imgSelected.layer.shadowColor = UIColor.systemPink.cgColor
        imgSelected.layer.masksToBounds = false
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            lblSelected.font = lblSelected.font.withSize(30)
        }
        
        for i in listLbl{
            i.textColor = #colorLiteral(red: 0.2705882353, green: 0.1921568627, blue: 0.1921568627, alpha: 1)
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            listLbl[0].font = listLbl[0].font.withSize(40)
            listLbl[1].font = listLbl[1].font.withSize(45)
            listLbl[2].font = listLbl[2].font.withSize(20)
        }
        
        listLbl[0].text = "Learn Chinese".localized()
        listLbl[1].text = "Choose your language".localized()
        listLbl[2].text = "Select the language to get started.".localized()
        outletBtnNextScreen.setTitle("Let get started".localized(), for: .normal)
    }
    
    
    @IBAction func btnNavi(_ sender: Any) {
        
//        if PaymentManager.shared.isPurchase(){
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
            self.navigationController?.pushViewController(vc, animated: true)
//        }else{
//            DispatchQueue.main.async {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
//            }
//        }
    }
    
    @IBAction func btnNextScreen(_ sender: Any) {
        
//        if PaymentManager.shared.isPurchase(){
            Sqldata.shared.loadInit(name: lblSelected.text!)
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeController") as! HomeController
            self.navigationController?.pushViewController(vc, animated: true)
            self.showSpinner(onView: self.view)
            UserDefaults.standard.set(nameLocal, forKey: "languageKey")
//        }else{
//            DispatchQueue.main.async {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
//            }
//        }
    }
    
    @IBAction func btnPotuguese(_ sender: Any) {
        self.lblSelected.text = "Portuguese"
        nameLocal = "pt_BR"
    }
    @IBAction func btnSpanish(_ sender: Any) {
        self.lblSelected.text = "Spanish"
        nameLocal = "es"
    }
    
    @IBAction func btnFrench(_ sender: Any) {
        self.lblSelected.text = "French"
        nameLocal = "fr"
    }
    @IBAction func btnGerrman(_ sender: Any) {
        self.lblSelected.text = "German"
        nameLocal = "de"
    }
    @IBAction func btnTurkish(_ sender: Any) {
        self.lblSelected.text = "Turkish"
        nameLocal = "tr"
    }
    @IBAction func btnJapanese(_ sender: Any) {
        self.lblSelected.text = "Japanese"
        nameLocal = "ja"
    }
    @IBAction func btnItalian(_ sender: Any) {
        self.lblSelected.text = "Italian"
        nameLocal = "it"
    }
    @IBAction func btnRussian(_ sender: Any) {
        self.lblSelected.text = "Russian"
        nameLocal = "ru"
    }
    @IBAction func btnEnglish(_ sender: Any) {
        self.lblSelected.text = "English"
        nameLocal = "en"
    }
    @IBAction func btnKorea(_ sender: Any) {
        self.lblSelected.text = "Korean"
        nameLocal = "ko"
    }
}

extension ViewController {
    
    func showSpinner(onView : UIView) {
            let spinnerView = UIView.init(frame: onView.bounds)
            spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
            let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
            ai.startAnimating()
            ai.center = spinnerView.center
        
            DispatchQueue.main.async {
                spinnerView.addSubview(ai)
                onView.addSubview(spinnerView)
            }
            vSpinner = spinnerView
        }
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}





