//
//  PhraseWordsVC.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/26/20.
//

import UIKit
import AVFoundation
import SwiftKeychainWrapper
import FBAudienceNetwork
import GoogleMobileAds


class PhraseWordsVC: UIViewController {
    var fbNativeAds: FBNativeAd?
        var admobNativeAds: GADUnifiedNativeAd?
    let scale = UIScreen.main.bounds.width / 414

    @IBOutlet weak var imgBg: UIImageView!
    
    var nameCate:String = ""
    var idCate:Int = 0
    
    var status = 0
    
    var listDataPhrase = [PhraseModel]()
    var listDataWords = [WordsModel]()
    
    var indexSelectP = -1
    var indexSelectW = -1
    
    var beforeIndexSelectP = -1
    var beforeIndexSelectW = -1
    let synth = AVSpeechSynthesizer()
    
    var isplayingP:Bool = true
    var isplayingW:Bool = true
    
    @IBOutlet weak var outletIconBack: UIButton!
    @IBOutlet weak var clv: UICollectionView!
    @IBOutlet weak var onBtnPhrase: UIButton!
    @IBOutlet weak var olBtnWords: UIButton!
    @IBOutlet var viewBg: UIView!
    @IBOutlet weak var lineWords: UIView!
    @IBOutlet weak var linePhrase: UIView!
    @IBOutlet weak var lblNameCategory: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAdFull(inVC: self)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
        clv.reloadData()
        let keyData = KeychainWrapper.standard.integer(forKey: "keyModeOn") ?? 0
        if UIDevice.current.userInterfaceIdiom == .pad{
            lblNameCategory.font = lblNameCategory.font.withSize(CGFloat(MyVariables.textSizeTitle))
            olBtnWords.titleLabel?.font = olBtnWords.titleLabel?.font.withSize(30)
            onBtnPhrase.titleLabel?.font = onBtnPhrase.titleLabel?.font.withSize(30)
            
            outletIconBack.setImage(UIImage(named: "icArrLeftWhite2x"), for: .normal)
            if keyData == 1 {
                imgBg.image = UIImage(named: "blackBg")
            } else {
                imgBg.image = UIImage(named: "Phrase3x")
            }
        } else {
            if keyData == 1 {
                imgBg.image = UIImage(named: "blackBg")
            } else {
                imgBg.image = UIImage(named: "Phrase")
            }
        }
        
        if PaymentManager.shared.isPurchase() {
            if fbNativeAds != nil{
                fbNativeAds = nil
                clv.reloadData()
            }
            if admobNativeAds != nil{
                admobNativeAds = nil
                clv.reloadData()
            }
        }else{
            if let native = AdmobManager.shared.randoomNativeAds(){
                if native is FBNativeAd{
                    fbNativeAds = native as? FBNativeAd
                    admobNativeAds = nil
                }else{
                    admobNativeAds = native as? GADUnifiedNativeAd
                    fbNativeAds = nil
                }
                clv.reloadData()
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        synth.delegate = self
                
        
        let scale = UIScreen.main.bounds.width / 414

        // Do any additional setup after loading the view.
        

        
        
  
        
        //Get NameCategory
        lblNameCategory.text = nameCate
        onBtnPhrase.setTitle("Phrases".localized(), for: .normal)
        olBtnWords.setTitle("Words".localized(), for: .normal)

        clv.backgroundColor = .clear
        clv.dataSource = self
        clv.delegate = self
        clv.register(UINib(nibName: "itemPhraseWords", bundle: nil), forCellWithReuseIdentifier: "itemPhraseWords")
        clv.register(UINib(nibName: "nativeAdmobCLVCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "nativeAdmobCLVCell")
                AdmobManager.shared.loadAllNativeAds()
        
        
        
        let layoutCollection : UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        if UIDevice.current.userInterfaceIdiom == .pad {
            layoutCollection.itemSize = CGSize(width: scale*360, height:scale*48)
        } else {
            layoutCollection.itemSize = CGSize(width: scale*360, height:scale*68)
        }
        
        layoutCollection.sectionInset.top = scale*34
        layoutCollection.sectionInset.right = scale*27
        layoutCollection.sectionInset.left = scale*27
        layoutCollection.sectionInset.bottom = scale*34
        layoutCollection.minimumLineSpacing = scale*20
        clv.collectionViewLayout = layoutCollection
        if let layout = clv.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
        status = 0
        setBgColorBtn()
        getData()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPhrases(_ sender: Any) {
        status = 0
        setBgColorBtn()
        getData()
        isplayingP = true
        clv.reloadData()
    }

    @IBAction func btnWords(_ sender: Any) {
        isplayingW = true
        status = 1
        setBgColorBtn()
        getData()
        clv.reloadData()
    }
    
    func getData(){
        if status == 0 {
            listDataPhrase = PhraseEntity.share.getData(idCate: idCate)
        } else {
            listDataWords = WordsEntity.share.getData(idCate: idCate)
        }
    }
    
    func setBgColorBtn(){
        if status == 0  {
            self.linePhrase.backgroundColor = .yellow
            self.onBtnPhrase.setTitleColor(.yellow, for: .normal)
            self.lineWords.backgroundColor = .white
            self.olBtnWords.setTitleColor(.white, for: .normal)
        } else {
            self.lineWords.backgroundColor = .yellow
            self.olBtnWords.setTitleColor(.yellow, for: .normal)
            self.linePhrase.backgroundColor = .white
            self.onBtnPhrase.setTitleColor(.white, for: .normal)
        }
    }
    
    func textToSpeech(text:String){
            let utterance = AVSpeechUtterance(string: text)
            utterance.voice = AVSpeechSynthesisVoice(language: "zh-CN")
            utterance.rate = 0.4
            synth.speak(utterance)
    }
    
}

extension PhraseWordsVC: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch status {
        case 0:
            return listDataPhrase.count
        case 1:
            return listDataWords.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = clv.dequeueReusableCell(withReuseIdentifier: "itemPhraseWords", for: indexPath) as! itemPhraseWords
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell.lblName.font = cell.lblName.font.withSize(CGFloat(MyVariables.textSizeItemCollectionView))
        }
        
        if status == 0 {
            cell.lblName.text = self.listDataPhrase[indexPath.row].nativeContent
            if self.indexSelectP == indexPath.row{
                print(String(isplayingP))
                if isplayingP {
                    synth.stopSpeaking(at: .immediate)
                    cell.viewBg.backgroundColor = UIColor.white
                    cell.imgBg.isHidden = true
                    cell.lblName.textColor = UIColor.black
                        cell.lblName.text = self.listDataPhrase[indexPath.row].nativeContent
                    isplayingP = !isplayingP

                } else {
                    cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                    cell.imgBg.isHidden = false
                        cell.lblName.textColor = UIColor.white
                            cell.lblName.text = self.listDataPhrase[indexSelectP].targetContent
                    beforeIndexSelectP = indexSelectP
                    textToSpeech(text: self.listDataPhrase[indexSelectP].targetContent)
                    isplayingP = !isplayingP

                }
                if beforeIndexSelectP != indexSelectP {
                    if isplayingP {
                        synth.stopSpeaking(at: .immediate)
                        isplayingP = !isplayingP
                        cell.viewBg.backgroundColor = UIColor.white
                        cell.imgBg.isHidden = true
                        cell.lblName.textColor = UIColor.black
                            cell.lblName.text = self.listDataPhrase[indexPath.row].nativeContent
                    } else {
                        cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                        cell.imgBg.isHidden = false
                            cell.lblName.textColor = UIColor.white
                                cell.lblName.text = self.listDataPhrase[indexSelectP].targetContent
                        beforeIndexSelectP = indexSelectP
                        textToSpeech(text: self.listDataPhrase[indexSelectP].targetContent)
                        isplayingP = !isplayingP
                    }
                }

            }else{
                cell.viewBg.backgroundColor = UIColor.white
                cell.imgBg.isHidden = true
                cell.lblName.textColor = UIColor.black
                    cell.lblName.text = self.listDataPhrase[indexPath.row].nativeContent
            }
            if listDataPhrase[indexPath.row].aux == 0 {
                cell.olbtnStar.setImage(UIImage(named: "icStarNotSelect"), for: .normal)
            } else {
                cell.olbtnStar.setImage(UIImage(named: "icStar"), for: .normal)
            }
            
        } else {
            cell.lblName.text = self.listDataWords[indexPath.row].nativeContent
            if self.indexSelectW == indexPath.row{
                if isplayingW {
                    synth.stopSpeaking(at: .immediate)
                    cell.viewBg.backgroundColor = UIColor.white
                    cell.imgBg.isHidden = true
                    cell.lblName.textColor = UIColor.black
                        cell.lblName.text = self.listDataWords[indexPath.row].nativeContent
                    isplayingW = !isplayingW

                } else {
                    cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                    cell.imgBg.isHidden = false
                        cell.lblName.textColor = UIColor.white
                            cell.lblName.text = self.listDataWords[indexSelectW].targetContent
                    beforeIndexSelectW = indexSelectW
                    textToSpeech(text: self.listDataWords[indexSelectW].targetContent)
                    isplayingW = !isplayingW

                }
                if beforeIndexSelectW != indexSelectW {
                    if isplayingW {
                        synth.stopSpeaking(at: .immediate)
                        isplayingW = !isplayingW
                        cell.viewBg.backgroundColor = UIColor.white
                        cell.imgBg.isHidden = true
                        cell.lblName.textColor = UIColor.black
                            cell.lblName.text = self.listDataWords[indexPath.row].nativeContent
                    } else {
                        cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                        cell.imgBg.isHidden = false
                            cell.lblName.textColor = UIColor.white
                                cell.lblName.text = self.listDataWords[indexSelectW].targetContent
                        beforeIndexSelectW = indexSelectW
                        textToSpeech(text: self.listDataWords[indexSelectW].targetContent)
                        isplayingW = !isplayingW
                    }
                }
            }else{
                cell.imgBg.isHidden = true
                cell.viewBg.backgroundColor = UIColor.white
                cell.lblName.textColor = UIColor.black
                    cell.lblName.text = self.listDataWords[indexPath.row].nativeContent
            }
            
            if listDataWords[indexPath.row].aux == 0 {
                cell.olbtnStar.setImage(UIImage(named: "icStarNotSelect"), for: .normal)
            } else {
                cell.olbtnStar.setImage(UIImage(named: "icStar"), for: .normal)
            }
            
        }
       
        
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.4
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
                
        
        cell.olbtnStar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap(_:))))
        
                
        return cell
     
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer) {

       let location = sender.location(in: self.clv)
       let indexPath = self.clv.indexPathForItem(at: location)
        
        print("sss")
        
        if status == 0 {
            indexSelectP = 100000
            let id = listDataPhrase[indexPath!.row].id
            let aux = listDataPhrase[indexPath!.row].aux
            PhraseEntity.share.updateAUX(idP: id,auxdata: aux)
            viewWillAppear(true)
        } else {
            indexSelectW = 100000
            let id = listDataWords[indexPath!.row].id
            let aux = listDataWords[indexPath!.row].aux
            WordsEntity.share.updateAUX(idP: id,auxdata: aux)
            viewWillAppear(true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if status == 0 {
            self.indexSelectP = indexPath.row
            self.clv.reloadData()
        } else {
            self.indexSelectW = indexPath.row
            self.clv.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader{
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "nativeAdmobCLVCell", for: indexPath) as! nativeAdmobCLVCell
                if let native = self.admobNativeAds {
                    headerView.setupHeader(nativeAd: native)
                }
                return headerView
            }
            return UICollectionReusableView()
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            if fbNativeAds == nil && admobNativeAds == nil{
                return CGSize(width: DEVICE_WIDTH, height: 0)
            }
            return CGSize(width: DEVICE_WIDTH, height: 150 * scale)
        }
}

extension PhraseWordsVC: AVSpeechSynthesizerDelegate{
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        print("Done")
    }
}
