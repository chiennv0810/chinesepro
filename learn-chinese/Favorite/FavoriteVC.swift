//
//  FavoriteVC.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 12/7/20.
//

import UIKit
import AVFoundation
import SwiftKeychainWrapper
import FBAudienceNetwork
import GoogleMobileAds
class FavoriteVC: UIViewController {
    
    var fbNativeAds: FBNativeAd?
    var admobNativeAds: GADUnifiedNativeAd?
    
    var status = 0
    var indexSelectP = -1
    var indexSelectW = -1
    var listFVPhrase = [PhraseModel]()
    var listFVWord = [WordsModel]()
    var beforeIndexSelectP = -1
    var beforeIndexSelectW = -1
    let synth = AVSpeechSynthesizer()
    
    var isplayingP:Bool = true
    var isplayingW:Bool = true
    
    let scale = UIScreen.main.bounds.width / 414

    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var outltetIcBack: UIButton!
    @IBOutlet weak var lblBookmark: UILabel!
    @IBOutlet weak var collectionviewData: UICollectionView!
    @IBOutlet weak var linePhrase: UIView!
    @IBOutlet weak var lineWords: UIView!
    @IBOutlet weak var btnPhrase: UIButton!
    @IBOutlet weak var btnWord: UIButton!
    @IBOutlet var viewBg: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAdFull(inVC: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let keyData = KeychainWrapper.standard.integer(forKey: "keyModeOn") ?? 0
        if UIDevice.current.userInterfaceIdiom == .pad {
            lblBookmark.font = lblBookmark.font.withSize(CGFloat(MyVariables.textSizeTitle))
            btnWord.titleLabel?.font = btnWord.titleLabel?.font.withSize(30)
            btnPhrase.titleLabel?.font = btnPhrase.titleLabel?.font.withSize(30)
            
            outltetIcBack.setImage(UIImage(named: "icArrLeftWhite2x"), for: .normal)
            if keyData == 1 {
                imgBg.image = UIImage(named: "blackBg")
            } else {
                imgBg.image = UIImage(named: "Phrase3x")

            }
        } else {
            if keyData == 1 {
                imgBg.image = UIImage(named: "blackBg")
            } else {
                imgBg.image = UIImage(named: "Phrase")
            }
        }
        if PaymentManager.shared.isPurchase() {
            if fbNativeAds != nil{
                fbNativeAds = nil
                collectionviewData.reloadData()
            }
            if admobNativeAds != nil{
                admobNativeAds = nil
                collectionviewData.reloadData()
            }
        }else{
            if let native = AdmobManager.shared.randoomNativeAds(){
                if native is FBNativeAd{
                    fbNativeAds = native as? FBNativeAd
                    admobNativeAds = nil
                }else{
                    admobNativeAds = native as? GADUnifiedNativeAd
                    fbNativeAds = nil
                }
                collectionviewData.reloadData()
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        lblBookmark.text = "Bookmark".localized()
        btnPhrase.setTitle("Phrases".localized(), for: .normal)
        btnWord.setTitle("Words".localized(), for: .normal)
        
        collectionviewData.backgroundColor = .clear
        collectionviewData.dataSource = self
        collectionviewData.delegate = self
        collectionviewData.register(UINib(nibName: "itemPhraseWords", bundle: nil), forCellWithReuseIdentifier: "itemPhraseWords")
        collectionviewData.register(UINib(nibName: "nativeAdmobCLVCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "nativeAdmobCLVCell")
                AdmobManager.shared.loadAllNativeAds()
        let layoutCollection : UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            layoutCollection.itemSize = CGSize(width: scale*360, height:scale*48)
        } else {
            layoutCollection.itemSize = CGSize(width: scale*360, height:scale*68)
        }
        layoutCollection.sectionInset.top = scale*34
        layoutCollection.sectionInset.right = scale*27
        layoutCollection.sectionInset.left = scale*27
        layoutCollection.sectionInset.bottom = scale*34
        layoutCollection.minimumLineSpacing = scale*20
        collectionviewData.collectionViewLayout = layoutCollection
        if let layout = collectionviewData.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
        status = 0
        setBgColorBtn()
        
        
    }
    
    func getData(){
        if status == 0  {
            listFVPhrase = PhraseEntity.share.getDataFavorite()
        } else {
            listFVWord = WordsEntity.share.getDataFavorite()
        }
    }
    
    func setBgColorBtn(){
        if status == 0  {
            self.linePhrase.backgroundColor = .yellow
            self.btnPhrase.setTitleColor(.yellow, for: .normal)
            self.lineWords.backgroundColor = .white
            self.btnWord.setTitleColor(.white, for: .normal)
            listFVPhrase = PhraseEntity.share.getDataFavorite()
        } else {
            self.lineWords.backgroundColor = .yellow
            self.btnWord.setTitleColor(.yellow, for: .normal)
            self.linePhrase.backgroundColor = .white
            self.btnPhrase.setTitleColor(.white, for: .normal)
            listFVWord = WordsEntity.share.getDataFavorite()
        }
    }
    
    func textToSpeech(text:String){
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: "zh-CN")
        utterance.rate = 0.4
        synth.speak(utterance)
    }
    
    @IBAction func acPhrase(_ sender: Any) {
        isplayingP = true
        status = 0
        setBgColorBtn()
        collectionviewData.reloadData()
    }
    
    @IBAction func acWords(_ sender: Any) {
        isplayingW = true
        status = 1
        setBgColorBtn()
        collectionviewData.reloadData()
    }
    @IBAction func acBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension FavoriteVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if status == 0 {
            return listFVPhrase.count
        } else {
            return listFVWord.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionviewData.dequeueReusableCell(withReuseIdentifier: "itemPhraseWords", for: indexPath) as! itemPhraseWords
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell.lblName.font = cell.lblName.font.withSize(CGFloat(MyVariables.textSizeItemCollectionView))
        }
        
        cell.olbtnStar.setImage(UIImage(named: "icStar"), for: .normal)
        if status == 0 {
            cell.lblName.text = self.listFVPhrase[indexPath.row].nativeContent
            if self.indexSelectP == indexPath.row{
                if isplayingP {
                    synth.stopSpeaking(at: .immediate)
                    cell.viewBg.backgroundColor = UIColor.white
                    cell.imgBg.isHidden = true
                    cell.lblName.textColor = UIColor.black
                        cell.lblName.text = self.listFVPhrase[indexPath.row].nativeContent
                    isplayingP = !isplayingP

                } else {
                    cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                    cell.imgBg.isHidden = false
                        cell.lblName.textColor = UIColor.white
                            cell.lblName.text = self.listFVPhrase[indexSelectP].targetContent
                    beforeIndexSelectP = indexSelectP
                    textToSpeech(text: self.listFVPhrase[indexSelectP].targetContent)
                    isplayingP = !isplayingP

                }
                if beforeIndexSelectP != indexSelectP {
                    if isplayingP {
                        synth.stopSpeaking(at: .immediate)
                        isplayingP = !isplayingP
                        cell.viewBg.backgroundColor = UIColor.white
                        cell.imgBg.isHidden = true
                        cell.lblName.textColor = UIColor.black
                            cell.lblName.text = self.listFVPhrase[indexPath.row].nativeContent
                    } else {
                        cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                        cell.imgBg.isHidden = false
                            cell.lblName.textColor = UIColor.white
                                cell.lblName.text = self.listFVPhrase[indexSelectP].targetContent
                        beforeIndexSelectP = indexSelectP
                        textToSpeech(text: self.listFVPhrase[indexSelectP].targetContent)
                        isplayingP = !isplayingP
                    }
                }
            }else{
                cell.viewBg.backgroundColor = UIColor.white
                cell.imgBg.isHidden = true
                cell.lblName.textColor = UIColor.black
                    cell.lblName.text = self.listFVPhrase[indexPath.row].nativeContent
                
            }
            
            if listFVPhrase[indexPath.row].aux == 0 {
                cell.olbtnStar.setImage(UIImage(named: "icStarNotSelect"), for: .normal)
            } else {
                cell.olbtnStar.setImage(UIImage(named: "icStar"), for: .normal)
            }
            
        } else {
            cell.lblName.text = self.listFVWord[indexPath.row].nativeContent
            if self.indexSelectW == indexPath.row{
                if isplayingW {
                    synth.stopSpeaking(at: .immediate)
                    cell.viewBg.backgroundColor = UIColor.white
                    cell.imgBg.isHidden = true
                    cell.lblName.textColor = UIColor.black
                        cell.lblName.text = self.listFVWord[indexPath.row].nativeContent
                    isplayingW = !isplayingW

                } else {
                    cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                    cell.imgBg.isHidden = false
                        cell.lblName.textColor = UIColor.white
                            cell.lblName.text = self.listFVWord[indexSelectW].targetContent
                    beforeIndexSelectW = indexSelectW
                    textToSpeech(text: self.listFVWord[indexSelectW].targetContent)
                    isplayingW = !isplayingW

                }
                if beforeIndexSelectW != indexSelectW {
                    if isplayingW {
                        synth.stopSpeaking(at: .immediate)
                        isplayingW = !isplayingW
                        cell.viewBg.backgroundColor = UIColor.white
                        cell.imgBg.isHidden = true
                        cell.lblName.textColor = UIColor.black
                            cell.lblName.text = self.listFVWord[indexPath.row].nativeContent
                    } else {
                        cell.imgBg.image = UIImage(named: "bgItemPhraseSelected")
                        cell.imgBg.isHidden = false
                            cell.lblName.textColor = UIColor.white
                                cell.lblName.text = self.listFVWord[indexSelectW].targetContent
                        beforeIndexSelectW = indexSelectW
                        textToSpeech(text: self.listFVWord[indexSelectW].targetContent)
                        isplayingW = !isplayingW
                    }
                }
                    
            }else{
                cell.imgBg.isHidden = true
                cell.viewBg.backgroundColor = UIColor.white
                cell.lblName.textColor = UIColor.black
                    cell.lblName.text = self.listFVWord[indexPath.row].nativeContent
                
            }
            
            if listFVWord[indexPath.row].aux == 0 {
                cell.olbtnStar.setImage(UIImage(named: "icStarNotSelect"), for: .normal)
            } else {
                cell.olbtnStar.setImage(UIImage(named: "icStar"), for: .normal)
            }
            
        }
       
        
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.4
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
        
        
        //Tap Icon Star
        cell.olbtnStar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap(_:))))
        
        return cell
    }
    
    
    //Func Tap IcStar
    @objc func tap(_ sender: UITapGestureRecognizer) {

       let location = sender.location(in: self.collectionviewData)
       let indexPath = self.collectionviewData.indexPathForItem(at: location)
        
        print("sss")
        
        if status == 0 {
            let id = listFVPhrase[indexPath!.row].id
            let aux = listFVPhrase[indexPath!.row].aux
            
            //Update aux theo id dc select
            PhraseEntity.share.updateAUX(idP: id,auxdata: aux)
            getData()
            collectionviewData.reloadData()
        } else {
            let id = listFVWord[indexPath!.row].id
            let aux = listFVWord[indexPath!.row].aux
            WordsEntity.share.updateAUX(idP: id,auxdata: aux)
            getData()
            collectionviewData.reloadData()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if status == 0 {
            self.indexSelectP = indexPath.row
            self.collectionviewData.reloadData()
        } else {
            self.indexSelectW = indexPath.row
            self.collectionviewData.reloadData()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader{
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "nativeAdmobCLVCell", for: indexPath) as! nativeAdmobCLVCell
                if let native = self.admobNativeAds {
                    headerView.setupHeader(nativeAd: native)
                }
                return headerView
            }
            return UICollectionReusableView()
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            if fbNativeAds == nil && admobNativeAds == nil{
                return CGSize(width: DEVICE_WIDTH, height: 0)
            }
            return CGSize(width: DEVICE_WIDTH, height: 150 * scale)
        }
}
