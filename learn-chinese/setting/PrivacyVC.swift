//
//  PrivacyVC.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 12/4/20.
//

import UIKit
import SwiftKeychainWrapper

class PrivacyVC: UIViewController {

    @IBOutlet weak var outletIcBack: UIButton!
    @IBOutlet weak var lblContent4: UILabel!
    @IBOutlet weak var lblContent3: UILabel!
    @IBOutlet weak var lblname2: UILabel!
    @IBOutlet weak var lblContent2: UILabel!
    @IBOutlet weak var lblContent1: UILabel!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var viewBg: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        lbltitle.text = "Privacy policy".localized()
        lblname.text = "Privacy Policy of Learn Chinese".localized()
        lblContent1.text = "This Application collects some Personal Data from its Users.".localized()
        lblContent2.text = "This document can be printed for reference by using the print command in the settings of any browser.".localized()
        lblname2.text = "POLICY SUMMARY".localized()
        lblContent4.text = "Data transfer abroad based on consent, Data transfer abroad based on standard contractual clauses, Data transfer from the EU and/or Switzerland to the U.S based on Privacy Shield, Data transfer to countries that guarantee European standards and Other legal basis for Data transfer abroad \nPersonal Data: various types of Data".localized()
        // Do any additional setup after loading the view.
        
        let keyData = KeychainWrapper.standard.integer(forKey: "keyModeOn") ?? 0

        if UIDevice.current.userInterfaceIdiom == .pad {
            outletIcBack.setImage(UIImage(named: "icArrLeftWhite2x"), for: .normal)
            lblname.font = lblname.font.withSize(50)
            lblname.font = lblname2.font.withSize(50)
            lbltitle.font = lbltitle.font.withSize(CGFloat(MyVariables.textSizeTitle))
            lblContent2.font = lblContent2.font.withSize(30)
            lblContent1.font = lblContent1.font.withSize(30)
            lblContent4.font = lblContent4.font.withSize(30)
            
            if keyData == 1 {
                viewBg.backgroundColor = .black
            } else {
                viewBg.backgroundColor = UIColor(patternImage: UIImage(named: "Home3x")!)
            }
        } else {
            
            if keyData == 1 {
                viewBg.backgroundColor = .black
            } else {
                self.viewBg.addBackground(name: "Home1")
            }
        }

    }
    
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
