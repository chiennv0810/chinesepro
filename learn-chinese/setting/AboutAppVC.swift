//
//  AboutAppVC.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 12/3/20.
//

import UIKit
import SwiftKeychainWrapper

class AboutAppVC: UIViewController {

    @IBOutlet weak var outletIcBack: UIButton!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var viewBg: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "About app".localized()
        lblName.text = "Free Language Service".localized()
        lblContent.text = "If you need to learn languages or simply translate some text, with our apps you can do it very easily. Here we leave the Diloya products at your disposal.".localized()

        // Do any additional setup after loading the view.
        let keyData = KeychainWrapper.standard.integer(forKey: "keyModeOn") ?? 0

        if UIDevice.current.userInterfaceIdiom == .pad {
            outletIcBack.setImage(UIImage(named: "icArrLeftWhite2x"), for: .normal)
            lblContent.font = lblContent.font.withSize(30)
            lblName.font = lblContent.font.withSize(50)
            lblTitle.font = lblContent.font.withSize(CGFloat(MyVariables.textSizeTitle))
            
            if keyData == 1 {
                viewBg.backgroundColor = .black
            } else {
                viewBg.backgroundColor = UIColor(patternImage: UIImage(named: "Home3x")!)
            }
        } else {
            if keyData == 1 {
                viewBg.backgroundColor = .black
            } else {
                self.viewBg.addBackground(name: "Home1")
            }
        }
        
    }
    
 
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
