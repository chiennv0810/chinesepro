//
//  SettingsVC.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/27/20.
//

import UIKit
import StoreKit
import MessageUI
import SwiftKeychainWrapper

class SettingsVC: UIViewController {
    let scale = UIScreen.main.bounds.width / 414

    
    var index:Int?

    
    var nameSection:[String] = ["General".localized(),"Other Settings".localized()]
    var imgST = [["stLanguage","stDarkmode","stBookmard"],["stRate","stAbout","stFeedBack","stShare","stContact","stPrivate"]]
    var nameST = [["Language".localized(),"Dark mode".localized(),"Bookmark words".localized()],["Rate this app".localized(),"About app".localized(),"Give feedback".localized(),"Share this app".localized(),"Contact us".localized(),"Privacy policy".localized()]]

    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var outletIcX: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var clv: UICollectionView!
    @IBOutlet var viewBg: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "Settings".localized()
        clv.backgroundColor = .clear
        clv.delegate = self
        clv.dataSource = self
        
        clv.register(UINib(nibName: "itemSettings", bundle: nil), forCellWithReuseIdentifier: "itemSettings")
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            outletIcX.setImage(UIImage(named: "icX2x"), for: .normal)
            lblTitle.font = lblTitle.font.withSize(CGFloat(MyVariables.textSizeTitle))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let keyData = KeychainWrapper.standard.integer(forKey: "keyModeOn") ?? 0
        print(keyData)
        if keyData == 1 {
            imgBg.image = UIImage(named: "blackBg")
        } else {
            if UIDevice.current.userInterfaceIdiom == .pad {
                imgBg.image = UIImage(named: "Home3x")

                
            } else {
                imgBg.image = UIImage(named: "assHome")

            }
        }
    }
    
    @IBAction func btnX(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension SettingsVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
        switch result {
        case .sent:
            print("did sent")
        default:
            print("eror sent")
        }
    }
}

extension SettingsVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
              let mail = MFMailComposeViewController()
              mail.mailComposeDelegate = self
              mail.setToRecipients(["loanthinguyen677657@gmail.com"])
              mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
              present(mail, animated: true)
          } else {
            
            // create the alert
                   let alert = UIAlertController(title: "Error", message: "Plese sign in account mail.", preferredStyle: UIAlertController.Style.alert)

                   // add an action (button)
                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                   // show the alert
                   self.present(alert, animated: true, completion: nil)
          }
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: scale*356, height:scale*40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: scale*30, left: scale*27, bottom: scale*30, right: scale*27)
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return scale*28
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return imgST.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgST[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clv.dequeueReusableCell(withReuseIdentifier: "itemSettings", for: indexPath) as! itemSettings
        
        cell.lblName.text = self.nameST[indexPath.section][indexPath.item]
        cell.img.image = UIImage(named: self.imgST[indexPath.section][indexPath.item])
        
        if indexPath.section == 0 {
            if indexPath.row == 1 {
                cell.clickSwitch.isHidden = false
                cell.icArrRight.isHidden = true
            }
        }
        
        //Ipad
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell.lblName.font = cell.lblName.font.withSize(35)
        }
        
        cell.delegate = self
        return cell
    }
    
    
    func reloadVC(){
        let homePageVC = storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = homePageVC
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                nextScreenSelectLanguagevVC()
            case 1:
                printST()
            case 2:
                nextScreenBookmark()
            default:
                return printST()
            }
        } else {
            switch indexPath.row {
            case 0:
                rateApp()
            case 1:
                nextScreenAboutApp()
            case 2:
                sendEmail()
            case 3:
                shareApp(sender: viewBg)
            case 4:
                sendEmail()
            case 5:
                privacy()
            default:
                return printST()
            }
        }
    }
    
    func printST(){
        print("Some Thing")
    }
    
    func nextScreenBookmark(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavoriteVC") as! FavoriteVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    
   
    
    func nextScreenAboutApp(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AboutAppVC") as! AboutAppVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func nextScreenSelectLanguagevVC(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLanguagevVC") as! SelectLanguagevVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func privacy(){
        print("privacy")
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func shareApp(sender:UIView){
        print("share")
        UIGraphicsBeginImageContext(view.frame.size)
                view.layer.render(in: UIGraphicsGetCurrentContext()!)
                let image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()

                let textToShare = "Check out my app"

                if let myWebsite = URL(string: "http://itunes.apple.com/app/idXXXXXXXXX") {//Enter link to your app here
                    let objectsToShare = [textToShare, myWebsite, image ?? #imageLiteral(resourceName: "app-logo")] as [Any]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                    //Excluded Activities
                    activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                    //

                    activityVC.popoverPresentationController?.sourceView = sender
                    self.present(activityVC, animated: true, completion: nil)
                }
    }
    
    
    
    func rateApp() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()

        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "appId") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: scale*40)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderViewSetting", for: indexPath) as? SectionHeaderViewSetting{
            sectionHeader.name.text = nameSection[indexPath.section]
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                sectionHeader.name.font = sectionHeader.name.font.withSize(45)
            }
            
            return sectionHeader
        }
        return UICollectionReusableView()
        
    }
}

extension SettingsVC: ItemSettingDelegate{
    func valueChange(sender: UISwitch) {
        if (sender.isOn == true){
            let saveKeymode:Bool =  KeychainWrapper.standard.set(1 , forKey: "keyModeOn")
            print(String(saveKeymode) + " on")
        }
           else{
            let saveKeymode:Bool = KeychainWrapper.standard.set(2 , forKey: "keyModeOn")
            print(String(saveKeymode)+" off")
        }
        viewWillAppear(true)
    }
}

