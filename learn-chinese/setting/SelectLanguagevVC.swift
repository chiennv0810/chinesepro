//
//  SelectLanguagevVC.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 12/1/20.
//

import UIKit

class SelectLanguagevVC: UIViewController {
    var vSpinner: UIView?
    var nameLocal:String = "ja"

    @IBOutlet weak var outletIcBack: UIButton!
    @IBOutlet weak var outletBtnDone: UIButton!
    @IBOutlet weak var lblChoose: UILabel!
    @IBOutlet weak var lblSelected: UILabel!
    @IBOutlet weak var imgBgSelected: UIImageView!
    @IBOutlet weak var lblLanguage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            outletIcBack.setImage(UIImage(named: "icArrLeftBlack2x"), for: .normal)
            outletBtnDone.titleLabel?.font = outletBtnDone.titleLabel?.font.withSize(25)
        }
        // Do any additional setup after loading the view.
        
        imgBgSelected.layer.shadowRadius = 8
        imgBgSelected.layer.shadowOffset = .zero
        imgBgSelected.layer.shadowOpacity = 0.7
        imgBgSelected.layer.shadowColor = UIColor.systemPink.cgColor
        imgBgSelected.layer.masksToBounds = false
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            lblSelected.font = lblSelected.font.withSize(30)
            lblChoose.font = lblChoose.font.withSize(CGFloat(MyVariables.textSizeTitle2))
            lblLanguage.font = lblLanguage.font.withSize(CGFloat(MyVariables.textSizeTitle))
        }
        
        lblLanguage.textColor = UIColor(red: 0.27, green: 0.19, blue: 0.19, alpha: 1)
        lblLanguage.text = "Language".localized()
        lblChoose.text = "Choose your language".localized()
        outletBtnDone.setTitle("Done".localized(), for: .normal)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFrench(_ sender: Any) {
        self.lblSelected.text = "French"
        nameLocal = "fr"
    }
    @IBAction func btnSpanish(_ sender: Any) {
        self.lblSelected.text = "Spanish"
        nameLocal = "es"
    }
    @IBAction func btnGerman(_ sender: Any) {
        self.lblSelected.text = "German"
        nameLocal = "de"
    }
    @IBAction func btnTurkish(_ sender: Any) {
        self.lblSelected.text = "Turkish"
        nameLocal = "tr"
    }
    @IBAction func btnJapanese(_ sender: Any) {
        self.lblSelected.text = "Japanese"
        nameLocal = "ja"
    }
    @IBAction func btnKorea(_ sender: Any) {
        self.lblSelected.text = "Korean"
        nameLocal = "ko"
    }
    @IBAction func btnPortuguese(_ sender: Any) {
        self.lblSelected.text = "Portuguese"
        nameLocal = "pt-BR"
    }
    @IBAction func btnEnglish(_ sender: Any) {
        self.lblSelected.text = "English"
        nameLocal = "en"
    }
    @IBAction func btnRussian(_ sender: Any) {
        self.lblSelected.text = "Russian"
        nameLocal = "ru"
    }
    @IBAction func btnItalian(_ sender: Any) {
        self.lblSelected.text = "Italian"
        nameLocal = "it"
    }
    @IBAction func btnDone(_ sender: Any) {
        Sqldata.shared.loadInit(name: lblSelected.text!)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeController") as! HomeController
        self.navigationController?.pushViewController(vc, animated: true)
        self.showSpinner(onView: self.view)
        UserDefaults.standard.set(nameLocal, forKey: "languageKey")

    }

}
extension SelectLanguagevVC {
    
    func showSpinner(onView : UIView) {
            let spinnerView = UIView.init(frame: onView.bounds)
            spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
            let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
            ai.startAnimating()
            ai.center = spinnerView.center
        
            DispatchQueue.main.async {
                spinnerView.addSubview(ai)
                onView.addSubview(spinnerView)
            }
            vSpinner = spinnerView
        }
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
