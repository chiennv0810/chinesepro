//
//  itemCategory.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/24/20.
//

import UIKit

class itemCategory: UICollectionViewCell {

  
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var imgCate: UIImageView!
    @IBOutlet weak var lblCateName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view.layer.cornerRadius = 15
    }
  
}
