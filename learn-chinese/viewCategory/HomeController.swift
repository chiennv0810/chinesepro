//
//  HomeController.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/24/20.
//

import UIKit
import SwiftKeychainWrapper

class HomeController: UIViewController {
    var listData = [CategoryModel]()
    var searchData = [CategoryModel]()
    var searching = false
        
    @IBOutlet weak var outletSelectTopic: UILabel!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var outletTranslate: UIButton!
    @IBOutlet weak var outletTwoline: UIButton!
    @IBOutlet weak var lblLearnChinese: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    let scale = UIScreen.main.bounds.width / 414
    
    @IBOutlet weak var collectionViewCate: UICollectionView!
    @IBOutlet var viewBg: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let keyData = KeychainWrapper.standard.integer(forKey: "keyModeOn") ?? 0

        

        if UIDevice.current.userInterfaceIdiom == .pad {
            lblLearnChinese.font = lblLearnChinese.font.withSize(CGFloat(MyVariables.textSizeTitle))
            
            if keyData == 1 {
                imgBg.image = UIImage(named: "blackBg")
            } else {
                imgBg.image = UIImage(named: "Home3x")
            }
        } else {
            if keyData == 1 {
                imgBg.image = UIImage(named: "blackBg")
            } else {
                imgBg.image = UIImage(named: "assHome")
            }
        }
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAdFull(inVC: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblLearnChinese.text = "Learn Chinese".localized()
        outletSelectTopic.text = "Select a topic you want to learn:".localized()
        
        AdmobManager.shared.loadBannerView(inVC: self)

        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField
            textFieldInsideUISearchBar?.textColor = UIColor.red
            textFieldInsideUISearchBar?.font = textFieldInsideUISearchBar?.font?.withSize(30)
            
            outletTwoline.setImage(UIImage(named: "twoline2xWhite"), for: .normal)
            outletTranslate.setImage(UIImage(named: "icTranslator2x"), for: .normal	)
            outletSelectTopic.font = outletSelectTopic.font.withSize(40)
        }
        
        
        

        collectionViewCate.dataSource = self
        collectionViewCate.delegate = self
        searchBar.delegate = self
        
        searchBar.placeholder = "Search".localized()
        searchBar.layer.borderColor = UIColor.white.cgColor
        searchBar.layer.borderWidth = 0.5
        searchBar.layer.cornerRadius = 10
        searchBar.clipsToBounds = true
        searchBar.tintColor = .white
        searchBar.setImage(UIImage(named: "icSearch"), for: .search, state: .normal)
        
        let forTf = searchBar.value(forKey: "searchField") as? UITextField
        forTf?.textColor = UIColor.white
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
                    textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
                }
        
        
        
        //GetDataCategory
        listData = CategoryEntity.shared.getData()
        print(listData.count)
                
        //ClearBackground Collection
        collectionViewCate.backgroundColor = .clear
        
        //setCollectionView
        collectionViewCate.register(UINib(nibName: "itemCategory", bundle: nil), forCellWithReuseIdentifier: "itemCategory")
        
        
        let layoutCollection : UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layoutCollection.itemSize = CGSize(width: scale*156.25, height:scale*250)
        collectionViewCate.collectionViewLayout = layoutCollection
        
    }
    
    

    @IBAction func btnTranslate(_ sender: Any) {
        if let url = URL(string: "https://translate.google.com") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func btnNavi(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomeController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate{
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return scale*30
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: scale*10, left: scale*36, bottom: scale*30, right: scale*36)
        }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searching {
            return searchData.count
        } else {
            return listData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewCate.dequeueReusableCell(withReuseIdentifier: "itemCategory", for: indexPath) as! itemCategory
        if searching {
            cell.lblCateName.text = self.searchData[indexPath.row].name
            cell.imgCate.image = UIImage(named: self.searchData[indexPath.row].image_name)
        } else {
            cell.lblCateName.text = self.listData[indexPath.row].name
            cell.imgCate.image = UIImage(named: self.listData[indexPath.row].image_name)
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell.lblCateName.font = cell.lblCateName.font.withSize(CGFloat(MyVariables.textSizeItemCollectionView))
        }
        
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.4
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
        
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchData = listData.filter({ (userData:CategoryModel) -> Bool in

            let data = userData.name.lowercased()
            if searchText != "" {
                return data.contains(searchText.lowercased())
            } else {
                return true
            }
        })

        print(searchText)

        if searchText != "" {
            searching = true
            self.collectionViewCate.reloadData()
        } else {
            searching = false
            self.collectionViewCate.reloadData()
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searching = true
        searchBar.endEditing(true)
        self.collectionViewCate.reloadData()

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(self.listData[indexPath.row].name)
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhraseWordsVC") as! PhraseWordsVC
        vc.nameCate = self.listData[indexPath.row].name
        vc.idCate = self.listData[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


