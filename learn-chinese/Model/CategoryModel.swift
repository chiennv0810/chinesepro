//
//  CategoryModel.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/25/20.
//

import Foundation
class CategoryModel{
    var id:Int = 0
    var name:String = ""
    var image_name:String = ""
    var image_info:String = ""
    init(id: Int, name: String, image_name: String, image_info: String) {
        self.id = id
        self.name = name
        self.image_name = image_name
        self.image_info = image_info
    }
    
}
