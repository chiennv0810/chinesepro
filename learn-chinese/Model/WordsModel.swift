//
//  WordsModel.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/26/20.
//

import Foundation
class WordsModel{
    var id:Int = 0
    var nativeContent:String = ""
    var targetContent:String = ""
    var idCategory:Int = 0
    var aux:Int = 0
    init(id:Int, nativeContent:String, targetContent:String, idCategory:Int, aux:Int) {
        self.id = id
        self.nativeContent = nativeContent
        self.targetContent = targetContent
        self.idCategory = idCategory
        self.aux = aux
    }
}
