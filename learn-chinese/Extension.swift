//
//  Extension.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 12/16/20.
//

import UIKit
import AVFoundation
import SwiftKeychainWrapper

class Service{
}

extension UIView{
    func addBackground(name:String){
        // screen width and height:
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height

        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageViewBackground.image = UIImage(named: name)

        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.scaleToFill

        self.addSubview(imageViewBackground)
        self.sendSubview(toBack: imageViewBackground)
    }
}

extension String {
    func localized() -> String {
        let lang = UserDefaults.standard.string(forKey: "languageKey") ?? ""
        print("$$$$$$$$$$$")
        print(lang)
        
        if let path = Bundle.main.path(forResource: lang, ofType: "lproj"){
            let bundle = Bundle(path: path)
            print(bundle!)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        } else {
            return self
        }
    }
}
