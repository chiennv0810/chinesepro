//
//  itemSettings.swift
//  learn-chinese
//
//  Created by Nguyen van chien on 11/27/20.
//

import UIKit
import SwiftKeychainWrapper

class itemSettings: UICollectionViewCell{
    
    
    @IBOutlet weak var clickSwitch: UISwitch!
    @IBOutlet weak var icArrRight: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var img: UIImageView!
    var delegate: ItemSettingDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let keyData = KeychainWrapper.standard.integer(forKey: "keyModeOn") ?? 0
        if keyData == 1 {
            clickSwitch.isOn = true
        } else {
            clickSwitch.isOn = false
        }
        
        clickSwitch.addTarget(self, action: #selector(valueChange), for: .valueChanged)
        
    
    }
    @objc func valueChange(_ sender: UISwitch){
        delegate?.valueChange(sender: sender)
    }
    override func prepareForReuse() {
        clickSwitch.isHidden = true
        icArrRight.isHidden = false
    }
}

protocol ItemSettingDelegate: class {
    func valueChange(sender:UISwitch)
}
